import { Component, ViewChild } from '@angular/core';
import { Platform, NavController, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import * as firebase from 'firebase';

import { TabsPage } from '../pages/tabs/tabs';
import { SettingsPage } from '../pages/settings/settings';
import { AuthPage } from '../pages/auth/auth';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  tabsPage: any = TabsPage;
  settingsPage: any = SettingsPage;
  authPage: any = AuthPage;
  @ViewChild('content') content: NavController;
  isAuth: boolean;

  constructor(platform: Platform, statusBar: StatusBar, 
    splashScreen: SplashScreen, private menuCtrl: MenuController) {
    platform.ready().then(() => {

      let config = {
        apiKey: "AIzaSyBjdEPIlXTBtrvrLMnoG3HvvsUO90eGZwk",
        authDomain: "lastactivitee.firebaseapp.com",
        databaseURL: "https://lastactivitee.firebaseio.com",
        projectId: "lastactivitee",
        storageBucket: "lastactivitee.appspot.com",
        messagingSenderId: "597173055634"
      };
      firebase.initializeApp(config);

      firebase.auth().onAuthStateChanged(
        (user) => {
          if(user){
            this.isAuth = true;
            this.content.setRoot(TabsPage);
          }else {
            this.isAuth = false;
            this.content.setRoot(AuthPage, {mode: 'loginMode'});
          }
        }
      );

      statusBar.styleDefault();
      splashScreen.hide();

    });
  }

  onNavigate(page: any, data?: {}) {
    this.content.setRoot(page, data ? data : null);
    this.menuCtrl.close();
  }

  onDisconnect() {
    firebase.auth().signOut();
    this.menuCtrl.close();
  }
}

