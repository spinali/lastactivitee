webpackJsonp([0],{

/***/ 139:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__cds_cdList_cdlist__ = __webpack_require__(245);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__books_booklist_booklist__ = __webpack_require__(248);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var TabsPage = /** @class */ (function () {
    function TabsPage() {
        this.cdListPage = __WEBPACK_IMPORTED_MODULE_0__cds_cdList_cdlist__["a" /* CdListPage */];
        this.bookListPage = __WEBPACK_IMPORTED_MODULE_1__books_booklist_booklist__["a" /* BookListPage */];
    }
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["m" /* Component */])({
            selector: 'page-tabs',template:/*ion-inline-start:"/home/spinali/Documents/js/lastactivitee/src/pages/tabs/tabs.html"*/'<ion-tabs>\n    <ion-tab [root]="bookListPage" tabTitle="Livres" tabIcon="book"></ion-tab>\n    <ion-tab [root]="cdListPage" tabTitle="CD" tabIcon="disc"></ion-tab>\n</ion-tabs>'/*ion-inline-end:"/home/spinali/Documents/js/lastactivitee/src/pages/tabs/tabs.html"*/
        })
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 148:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 148;

/***/ }),

/***/ 189:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 189;

/***/ }),

/***/ 244:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_firebase__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AuthService = /** @class */ (function () {
    function AuthService() {
        var _this = this;
        this.isAuth = false;
        __WEBPACK_IMPORTED_MODULE_0_firebase__["auth"]().onAuthStateChanged(function (user) {
            if (user) {
                _this.isAuth = true;
            }
            else {
                _this.isAuth = false;
            }
        });
    }
    AuthService.prototype.signUpUser = function (email, password) {
        return new Promise(function (resolve, reject) {
            __WEBPACK_IMPORTED_MODULE_0_firebase__["auth"]().createUserWithEmailAndPassword(email, password).then(function (user) {
                resolve(user);
            }, function (error) {
                reject(error);
            });
        });
    };
    AuthService.prototype.signInUser = function (email, password) {
        return new Promise(function (resolve, reject) {
            __WEBPACK_IMPORTED_MODULE_0_firebase__["auth"]().signInWithEmailAndPassword(email, password).then(function (user) {
                resolve(user);
            }, function (error) {
                reject(error);
            });
        });
    };
    AuthService.prototype.signOut = function () {
        __WEBPACK_IMPORTED_MODULE_0_firebase__["auth"]().signOut();
    };
    AuthService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], AuthService);
    return AuthService;
}());

//# sourceMappingURL=authService.js.map

/***/ }),

/***/ 245:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CdListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_donneesService__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__lendcd_lendcd__ = __webpack_require__(246);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CdListPage = /** @class */ (function () {
    function CdListPage(menuCtrl, modalCtrl, donneesService) {
        this.menuCtrl = menuCtrl;
        this.modalCtrl = modalCtrl;
        this.donneesService = donneesService;
    }
    CdListPage.prototype.ngOnInit = function () {
        var _this = this;
        this.cdSubscription = this.donneesService.cds$.subscribe(function (cds) {
            _this.cds = cds;
        });
        this.donneesService.fetchCdList();
    };
    CdListPage.prototype.ionViewWillEnter = function () {
        this.cds = this.donneesService.CdList.slice();
    };
    CdListPage.prototype.onToggleMenu = function () {
        this.menuCtrl.open();
    };
    CdListPage.prototype.onShowCd = function (index) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__lendcd_lendcd__["a" /* LendCdPage */], { index: index });
        modal.present();
    };
    CdListPage.prototype.ngOnDestroy = function () {
        this.cdSubscription.unsubscribe();
    };
    CdListPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'tab-cdlist',template:/*ion-inline-start:"/home/spinali/Documents/js/lastactivitee/src/pages/cds/cdList/cdlist.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-buttons>\n            <button ion-button icon-only (click)="onToggleMenu()">\n                <ion-icon name="menu"></ion-icon>   \n            </button>\n        </ion-buttons>\n        <ion-title>\n            Liste des CDs\n        </ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n    <button ion-item\n        icon-start\n        *ngFor="let cd of cds; let i = index"\n        [color]="cd.isLent ? \'unavailable\' : \'available-list\'"\n        (click)="onShowCd(i)">\n        <ion-icon name="disc"></ion-icon>\n        <b>{{ cd.title }}</b> <br />Artiste: {{ cd.artist }}\n        <p *ngIf="cd.borrower">Prêté à {{ cd.borrower.name }}</p>\n    </button>\n</ion-content>'/*ion-inline-end:"/home/spinali/Documents/js/lastactivitee/src/pages/cds/cdList/cdlist.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_2__services_donneesService__["a" /* DonneesService */]])
    ], CdListPage);
    return CdListPage;
}());

//# sourceMappingURL=cdlist.js.map

/***/ }),

/***/ 246:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LendCdPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_models_Borrower__ = __webpack_require__(247);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_donneesService__ = __webpack_require__(43);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LendCdPage = /** @class */ (function () {
    function LendCdPage(navParams, viewCtrl, donneesService, formBuilder) {
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.donneesService = donneesService;
        this.formBuilder = formBuilder;
        this.isLending = false;
    }
    LendCdPage.prototype.ngOnInit = function () {
        this.index = this.navParams.get('index');
        this.cd = this.donneesService.CdList[this.index];
        this.initForm();
    };
    LendCdPage.prototype.initForm = function () {
        this.borrowForm = this.formBuilder.group({
            name: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required]
        });
    };
    LendCdPage.prototype.onDismissModal = function () {
        this.viewCtrl.dismiss();
    };
    LendCdPage.prototype.onToggleLend = function () {
        if (this.cd.isLent) {
            this.donneesService.renderCd(this.cd);
            this.onDismissModal();
        }
        else {
            this.isLending = true;
        }
    };
    LendCdPage.prototype.onSubmitForm = function () {
        var newBorrower = new __WEBPACK_IMPORTED_MODULE_3__app_models_Borrower__["a" /* Borrower */](this.borrowForm.get('name').value);
        this.donneesService.lendCd(this.cd, newBorrower);
        this.onDismissModal();
    };
    LendCdPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-lendcd',template:/*ion-inline-start:"/home/spinali/Documents/js/lastactivitee/src/pages/cds/lendcd/lendcd.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-buttons>\n            <button ion-button clear (click)="onDismissModal()">Fermer</button>\n        </ion-buttons>\n    </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n    <ion-card [ngClass]="{\'is-on\': !cd.isLent, \'is-off\': cd.isLent }">\n        <ion-card-header>\n            {{ cd.title }}\n        </ion-card-header>\n        <ion-card-content>\n            <h3>{{ cd.artist }}</h3>\n            <p *ngFor="let line of cd.description">{{ line }}</p>\n            <br />\n            <h4 *ngIf="cd.isLent">\n                Cd prêté\n                <span *ngIf="cd.borrower">&nbsp;à {{ cd.borrower.name }}</span>\n            </h4>\n            <h4 *ngIf="!cd.isLent">Cd non prêté</h4>\n        </ion-card-content>\n    </ion-card>\n    <ion-grid>\n        <ion-row>\n            <ion-col>\n                <button ion-button full [disabled]="cd.isLent" color="unavailable" (click)="onToggleLend()">\n                    Prêter\n                </button>\n            </ion-col>\n            <ion-col>\n                <button ion-button full color="available" [disabled]="!cd.isLent" (click)="onToggleLend()">\n                    Rendre\n                </button>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n    <form [formGroup]="borrowForm" *ngIf="isLending">\n        <ion-list>\n            <ion-item>\n                <ion-label floating>Nom de l\'emprunteur</ion-label>\n                <ion-input formControlName="name"></ion-input>\n            </ion-item>\n        </ion-list>\n        <button ion-button full [disabled]="borrowForm.invalid" (click)="onSubmitForm()">Enregistrer</button>\n    </form>\n</ion-content>'/*ion-inline-end:"/home/spinali/Documents/js/lastactivitee/src/pages/cds/lendcd/lendcd.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_4__services_donneesService__["a" /* DonneesService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */]])
    ], LendCdPage);
    return LendCdPage;
}());

//# sourceMappingURL=lendcd.js.map

/***/ }),

/***/ 247:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Borrower; });
var Borrower = /** @class */ (function () {
    function Borrower(name) {
        this.name = name;
        this.isOn = true;
        this.borrowDate = new Date();
    }
    return Borrower;
}());

//# sourceMappingURL=Borrower.js.map

/***/ }),

/***/ 248:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BookListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_donneesService__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__lendbook_lendbook__ = __webpack_require__(249);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var BookListPage = /** @class */ (function () {
    function BookListPage(menuCtrl, modalCtrl, donneesService) {
        this.menuCtrl = menuCtrl;
        this.modalCtrl = modalCtrl;
        this.donneesService = donneesService;
    }
    BookListPage.prototype.ngOnInit = function () {
        var _this = this;
        this.bookSubscription = this.donneesService.books$.subscribe(function (books) {
            _this.books = books;
        });
        this.donneesService.fetchBookList();
    };
    BookListPage.prototype.ionViewWillEnter = function () {
        this.books = this.donneesService.bookList.slice();
    };
    BookListPage.prototype.onToggleMenu = function () {
        this.menuCtrl.open();
    };
    BookListPage.prototype.onShowBook = function (index) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__lendbook_lendbook__["a" /* LendBookPage */], { index: index });
        modal.present();
    };
    BookListPage.prototype.ngOnDestroy = function () {
        this.bookSubscription.unsubscribe();
    };
    BookListPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'tab-booklist',template:/*ion-inline-start:"/home/spinali/Documents/js/lastactivitee/src/pages/books/booklist/booklist.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-buttons>\n            <button ion-button icon-only (click)="onToggleMenu()">\n                <ion-icon name="menu"></ion-icon>\n            </button>\n        </ion-buttons>\n        <ion-title>\n            Liste des Livres\n        </ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n    <button ion-item\n        icon-start\n        *ngFor="let book of books; let i = index"\n        [color]="book.isLent ? \'unavailable\' : \'available-list\'"\n        (click)="onShowBook(i)">\n        <ion-icon name="book"></ion-icon>\n        <b>{{ book.title }}</b> <br />Auteur: {{ book.author }}\n        <p *ngIf="book.borrower">Prêté à {{ book.borrower.name }}</p>\n    </button>\n</ion-content>'/*ion-inline-end:"/home/spinali/Documents/js/lastactivitee/src/pages/books/booklist/booklist.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_2__services_donneesService__["a" /* DonneesService */]])
    ], BookListPage);
    return BookListPage;
}());

//# sourceMappingURL=booklist.js.map

/***/ }),

/***/ 249:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LendBookPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_models_Borrower__ = __webpack_require__(247);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_donneesService__ = __webpack_require__(43);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LendBookPage = /** @class */ (function () {
    function LendBookPage(navParams, viewCtrl, donneesService, formBuilder) {
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.donneesService = donneesService;
        this.formBuilder = formBuilder;
        this.isLending = false;
    }
    LendBookPage.prototype.ngOnInit = function () {
        this.index = this.navParams.get('index');
        this.book = this.donneesService.bookList[this.index];
        this.initForm();
    };
    LendBookPage.prototype.initForm = function () {
        this.borrowForm = this.formBuilder.group({
            name: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required]
        });
    };
    LendBookPage.prototype.onDismissModal = function () {
        this.viewCtrl.dismiss();
    };
    LendBookPage.prototype.onToggleLend = function () {
        if (this.book.isLent) {
            this.donneesService.renderBook(this.book);
            this.onDismissModal();
        }
        else {
            this.isLending = true;
        }
    };
    LendBookPage.prototype.onSubmitForm = function () {
        var newBorrower = new __WEBPACK_IMPORTED_MODULE_3__app_models_Borrower__["a" /* Borrower */](this.borrowForm.get('name').value);
        this.donneesService.lendBook(this.book, newBorrower);
        this.onDismissModal();
    };
    LendBookPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-lendbook',template:/*ion-inline-start:"/home/spinali/Documents/js/lastactivitee/src/pages/books/lendbook/lendbook.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-buttons>\n            <button ion-button clear (click)="onDismissModal()">Fermer</button>\n        </ion-buttons>\n    </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n    <ion-card [ngClass]="{\'is-on\': !book.isLent, \'is-off\': book.isLent }">\n        <ion-card-header>\n            {{ book.title }}\n        </ion-card-header>\n        <ion-card-content>\n            <h3>{{ book.author }}</h3>\n            <p *ngFor="let line of book.description">{{ line }}</p>\n            <br />\n            <h4 *ngIf="book.isLent">Livre prêté\n                <span *ngIf="book.borrower">&nbsp; à {{ book.borrower.name }}</span>\n            </h4>\n            <h4 *ngIf="!book.isLent">Livre non prêté</h4>\n        </ion-card-content>\n    </ion-card>\n    <ion-grid>\n        <ion-row>\n            <ion-col>\n                <button ion-button (click)="onToggleLend()" full [disabled]="book.isLent" color="unavailable">Prêter</button>\n            </ion-col>\n            <ion-col>\n                <button ion-button (click)="onToggleLend()" full [disabled]="!book.isLent" color="available">Rendre</button>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n    <form [formGroup]="borrowForm" *ngIf="isLending">\n        <ion-list>\n            <ion-item>\n                <ion-label floating>Nom de l\'emprunteur</ion-label>\n                <ion-input formControlName="name"></ion-input>\n            </ion-item>\n        </ion-list>\n        <button ion-button full [disabled]="borrowForm.invalid" (click)="onSubmitForm()">Enregistrer</button>\n    </form>\n</ion-content>'/*ion-inline-end:"/home/spinali/Documents/js/lastactivitee/src/pages/books/lendbook/lendbook.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_4__services_donneesService__["a" /* DonneesService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */]])
    ], LendBookPage);
    return LendBookPage;
}());

//# sourceMappingURL=lendbook.js.map

/***/ }),

/***/ 250:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_donneesService__ = __webpack_require__(43);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SettingsPage = /** @class */ (function () {
    function SettingsPage(menuCtrl, toastCtrl, loadingCtrl, donneesService) {
        this.menuCtrl = menuCtrl;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.donneesService = donneesService;
    }
    SettingsPage.prototype.onToggleMenu = function () {
        this.menuCtrl.open();
    };
    SettingsPage.prototype.onSaveList = function () {
        var _this = this;
        var loaderBook = this.loadingCtrl.create({
            content: 'Sauvegarde des données de livre en ligne en cours...'
        });
        loaderBook.present();
        this.donneesService.saveBooksToFirebase().then(function () {
            loaderBook.dismiss();
            _this.toastCtrl.create({
                message: 'Données de livre sauvegardées !',
                duration: 3000,
                position: 'bottom'
            }).present();
        }, function (error) {
            loaderBook.dismiss();
            _this.toastCtrl.create({
                message: error,
                duration: 3000,
                position: 'bottom'
            }).present();
        });
        var loaderCd = this.loadingCtrl.create({
            content: 'Sauvegarde des données de cd en ligne en cours...'
        });
        this.donneesService.saveCdsToFirebase()
            .then(function () {
            loaderCd.dismiss();
            _this.toastCtrl.create({
                message: 'Données de cd sauvegardées !',
                duration: 3000,
                position: 'bottom'
            }).present();
        }, function (error) {
            loaderCd.dismiss();
            _this.toastCtrl.create({
                message: error,
                duration: 3000,
                position: 'bottom'
            }).present();
        });
    };
    SettingsPage.prototype.onFetchList = function () {
        var _this = this;
        var loaderBook = this.loadingCtrl.create({
            content: 'Récupération des données de livre en cours...'
        });
        loaderBook.present();
        this.donneesService.retrieveBooksFromFirebase().then(function () {
            loaderBook.dismiss();
            _this.toastCtrl.create({
                message: 'Données de livre récupérées avec succès!',
                duration: 3000,
                position: 'bottom'
            }).present();
        }, function (error) {
            loaderBook.dismiss();
            _this.toastCtrl.create({
                message: error,
                duration: 3000,
                position: 'bottom'
            }).present();
        });
        var loaderCd = this.loadingCtrl.create({
            content: 'Récupération des données de cd en cours...'
        });
        loaderCd.present();
        this.donneesService.retrieveCdsFromFirebase().then(function () {
            loaderCd.dismiss();
            _this.toastCtrl.create({
                message: 'Données de cd récupérées avec succès!',
                duration: 3000,
                position: 'bottom'
            }).present();
        }, function (error) {
            loaderCd.dismiss();
            _this.toastCtrl.create({
                message: error,
                duration: 3000,
                position: 'bottom'
            }).present();
        });
    };
    SettingsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-settings',template:/*ion-inline-start:"/home/spinali/Documents/js/lastactivitee/src/pages/settings/settings.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-buttons>\n            <button ion-button icon-only (click)="onToggleMenu()">\n                <ion-icon name="menu"></ion-icon>\n            </button>\n        </ion-buttons>\n        <ion-title>\n            Réglages\n        </ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n    <ion-card>\n        <ion-card-header>Données en ligne</ion-card-header>\n        <ion-card-content>\n            <button ion-button block outline (click)="onSaveList()">Sauvegarder</button>\n            <button ion-button block outline (click)="onFetchList()">Récupérer</button>\n        </ion-card-content>\n    </ion-card>\n</ion-content>'/*ion-inline-end:"/home/spinali/Documents/js/lastactivitee/src/pages/settings/settings.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["e" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["j" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["d" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_2__services_donneesService__["a" /* DonneesService */]])
    ], SettingsPage);
    return SettingsPage;
}());

//# sourceMappingURL=settings.js.map

/***/ }),

/***/ 251:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_authService__ = __webpack_require__(244);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__tabs_tabs__ = __webpack_require__(139);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AuthPage = /** @class */ (function () {
    function AuthPage(navCtrl, navParams, menuCtrl, formBuilder, authService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.menuCtrl = menuCtrl;
        this.formBuilder = formBuilder;
        this.authService = authService;
    }
    AuthPage.prototype.ngOnInit = function () {
        this.mode = this.navParams.get('mode');
        this.initForm();
    };
    AuthPage.prototype.onToggleMenu = function () {
        this.menuCtrl.open();
    };
    AuthPage.prototype.initForm = function () {
        this.authForm = this.formBuilder.group({
            email: ['', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].email, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required]],
            password: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required]
        });
    };
    AuthPage.prototype.onSubmitForm = function () {
        var _this = this;
        var email = this.authForm.get('email').value;
        var password = this.authForm.get('password').value;
        if (this.mode === 'registerMode') {
            this.authService.signUpUser(email, password).then(function () {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__tabs_tabs__["a" /* TabsPage */]);
            }, function (error) {
                _this.errorMessage = error;
            });
        }
        else if (this.mode === 'loginMode') {
            this.authService.signInUser(email, password).then(function () {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__tabs_tabs__["a" /* TabsPage */]);
            }, function (error) {
                _this.errorMessage = error;
            });
        }
    };
    AuthPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-auth',template:/*ion-inline-start:"/home/spinali/Documents/js/lastactivitee/src/pages/auth/auth.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-buttons start>\n      <button ion-button icon-only (click)="onToggleMenu()">\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title *ngIf="mode === \'registerMode\'">Inscription</ion-title>\n    <ion-title *ngIf="mode === \'loginMode\'">Connexion</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <form [formGroup]="authForm">\n    <ion-list>\n      <ion-item>\n        <ion-label floating>Adresse mail</ion-label>\n        <ion-input type="email" formControlName="email"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating>Mot de passe</ion-label>\n        <ion-input type="password" formControlName="password"></ion-input>\n      </ion-item>\n    </ion-list>\n    <button ion-button full (click)="onSubmitForm()">Valider</button>\n    <span ion-text color="danger">{{ errorMessage }}</span>\n  </form>\n</ion-content>\n'/*ion-inline-end:"/home/spinali/Documents/js/lastactivitee/src/pages/auth/auth.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_3__services_authService__["a" /* AuthService */]])
    ], AuthPage);
    return AuthPage;
}());

//# sourceMappingURL=auth.js.map

/***/ }),

/***/ 252:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(253);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(275);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 275:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(229);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(232);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(233);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_donneesService__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_authService__ = __webpack_require__(244);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_component__ = __webpack_require__(380);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_settings_settings__ = __webpack_require__(250);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_cds_lendcd_lendcd__ = __webpack_require__(246);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_cds_cdList_cdlist__ = __webpack_require__(245);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_books_lendbook_lendbook__ = __webpack_require__(249);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_tabs_tabs__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_home_home__ = __webpack_require__(381);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_books_booklist_booklist__ = __webpack_require__(248);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_auth_auth__ = __webpack_require__(251);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_14__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_books_booklist_booklist__["a" /* BookListPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_books_lendbook_lendbook__["a" /* LendBookPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_cds_cdList_cdlist__["a" /* CdListPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_cds_lendcd_lendcd__["a" /* LendCdPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_settings_settings__["a" /* SettingsPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_auth_auth__["a" /* AuthPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* MyApp */], {}, {
                    links: []
                }),
                __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["a" /* IonicStorageModule */].forRoot()
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_14__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_books_booklist_booklist__["a" /* BookListPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_books_lendbook_lendbook__["a" /* LendBookPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_cds_cdList_cdlist__["a" /* CdListPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_cds_lendcd_lendcd__["a" /* LendCdPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_settings_settings__["a" /* SettingsPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_auth_auth__["a" /* AuthPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_6__services_donneesService__["a" /* DonneesService */],
                __WEBPACK_IMPORTED_MODULE_7__services_authService__["a" /* AuthService */],
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicErrorHandler */] },
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 380:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(232);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(229);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebase__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_tabs_tabs__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_settings_settings__ = __webpack_require__(250);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_auth_auth__ = __webpack_require__(251);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, menuCtrl) {
        var _this = this;
        this.menuCtrl = menuCtrl;
        this.tabsPage = __WEBPACK_IMPORTED_MODULE_5__pages_tabs_tabs__["a" /* TabsPage */];
        this.settingsPage = __WEBPACK_IMPORTED_MODULE_6__pages_settings_settings__["a" /* SettingsPage */];
        this.authPage = __WEBPACK_IMPORTED_MODULE_7__pages_auth_auth__["a" /* AuthPage */];
        platform.ready().then(function () {
            var config = {
                apiKey: "AIzaSyBjdEPIlXTBtrvrLMnoG3HvvsUO90eGZwk",
                authDomain: "lastactivitee.firebaseapp.com",
                databaseURL: "https://lastactivitee.firebaseio.com",
                projectId: "lastactivitee",
                storageBucket: "lastactivitee.appspot.com",
                messagingSenderId: "597173055634"
            };
            __WEBPACK_IMPORTED_MODULE_4_firebase__["initializeApp"](config);
            __WEBPACK_IMPORTED_MODULE_4_firebase__["auth"]().onAuthStateChanged(function (user) {
                if (user) {
                    _this.isAuth = true;
                    _this.content.setRoot(__WEBPACK_IMPORTED_MODULE_5__pages_tabs_tabs__["a" /* TabsPage */]);
                }
                else {
                    _this.isAuth = false;
                    _this.content.setRoot(__WEBPACK_IMPORTED_MODULE_7__pages_auth_auth__["a" /* AuthPage */], { mode: 'loginMode' });
                }
            });
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp.prototype.onNavigate = function (page, data) {
        this.content.setRoot(page, data ? data : null);
        this.menuCtrl.close();
    };
    MyApp.prototype.onDisconnect = function () {
        __WEBPACK_IMPORTED_MODULE_4_firebase__["auth"]().signOut();
        this.menuCtrl.close();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('content'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */])
    ], MyApp.prototype, "content", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/home/spinali/Documents/js/lastactivitee/src/app/app.html"*/'<ion-menu [content]="content">\n    <ion-header>\n        <ion-toolbar>\n            <ion-title>Menu</ion-title>\n        </ion-toolbar>\n    </ion-header>\n    <ion-content>\n        <ion-list>\n            <button ion-item icon-start (click)="onNavigate(tabsPage)" *ngIf="isAuth">\n                <ion-icon name="book"></ion-icon>\n                Livres & Cd\n            </button>\n            <button ion-item icon-start (click)="onNavigate(settingsPage)" *ngIf="isAuth">\n                <ion-icon name="settings"></ion-icon>\n                Réglages\n            </button>\n            <button ion-item icon-start (click)="onNavigate(authPage, { mode: \'registerMode\'})" *ngIf="!isAuth">\n                <ion-icon name="person-add"></ion-icon>\n                Inscription\n            </button>\n            <button ion-item icon-start (click)="onNavigate(authPage, { mode: \'loginMode\'})" *ngIf="!isAuth">\n                <ion-icon name="person"></ion-icon>\n                Connexion\n            </button>\n            <button ion-item icon-start (click)="onDisconnect()" *ngIf="isAuth">\n                <ion-icon name="exit"></ion-icon>\n                Déconnexion\n            </button>\n        </ion-list>\n    </ion-content>\n</ion-menu>\n<ion-nav [root]="tabsPage" #content></ion-nav>\n'/*ion-inline-end:"/home/spinali/Documents/js/lastactivitee/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* MenuController */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 381:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HomePage = /** @class */ (function () {
    function HomePage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/home/spinali/Documents/js/lastactivitee/src/pages/home/home.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      Ionic Blank\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  The world is your oyster.\n  <p>\n    If you get lost, the <a href="http://ionicframework.com/docs/v2">docs</a> will be your guide.\n  </p>\n</ion-content>\n'/*ion-inline-end:"/home/spinali/Documents/js/lastactivitee/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 43:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DonneesService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ionic_storage__ = __webpack_require__(233);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_firebase__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DonneesService = /** @class */ (function () {
    function DonneesService(storage) {
        this.storage = storage;
        this.books$ = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__["Subject"]();
        this.cds$ = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__["Subject"]();
        this.bookList = [
            {
                title: 'Les Trois Mousquetaires',
                author: 'Alexandre Dumas',
                description: [
                    'synopsis: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi. Proin porttitor, orci nec nonummy molestie, enim est eleifend mi, non fermentum diam nisl sit amet erat. Duis semper. Duis arcu massa, scelerisque vitae, consequat in, pretium a, enim. Pellentesque congue. Ut in risus volutpat libero pharetra tempor. Cras vestibulum bibendum augue. Praesent egestas leo in pede. Praesent blandit odio eu enim. Pellentesque sed dui ut augue blandit sodales. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam nibh. Mauris ac mauris sed pede pellentesque fermentum. Maecenas adipiscing ante non diam sodales hendrerit.'
                ],
                isLent: false
            },
            {
                title: 'Le Comte de Monte-Cristo',
                author: 'Alexandre Dumas',
                description: [
                    'synopsis: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi. Proin porttitor, orci nec nonummy molestie, enim est eleifend mi, non fermentum diam nisl sit amet erat. Duis semper. Duis arcu massa, scelerisque vitae, consequat in, pretium a, enim. Pellentesque congue. Ut in risus volutpat libero pharetra tempor. Cras vestibulum bibendum augue. Praesent egestas leo in pede. Praesent blandit odio eu enim. Pellentesque sed dui ut augue blandit sodales. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam nibh. Mauris ac mauris sed pede pellentesque fermentum. Maecenas adipiscing ante non diam sodales hendrerit.'
                ],
                isLent: false
            }, {
                title: 'La Reine Margot',
                author: 'Alexandre Dumas',
                description: [
                    'synopsis: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi. Proin porttitor, orci nec nonummy molestie, enim est eleifend mi, non fermentum diam nisl sit amet erat. Duis semper. Duis arcu massa, scelerisque vitae, consequat in, pretium a, enim. Pellentesque congue. Ut in risus volutpat libero pharetra tempor. Cras vestibulum bibendum augue. Praesent egestas leo in pede. Praesent blandit odio eu enim. Pellentesque sed dui ut augue blandit sodales. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam nibh. Mauris ac mauris sed pede pellentesque fermentum. Maecenas adipiscing ante non diam sodales hendrerit.'
                ],
                isLent: false
            }, {
                title: 'La dame de Monsoreau',
                author: 'Alexandre Dumas',
                description: [
                    'synopsis: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi. Proin porttitor, orci nec nonummy molestie, enim est eleifend mi, non fermentum diam nisl sit amet erat. Duis semper. Duis arcu massa, scelerisque vitae, consequat in, pretium a, enim. Pellentesque congue. Ut in risus volutpat libero pharetra tempor. Cras vestibulum bibendum augue. Praesent egestas leo in pede. Praesent blandit odio eu enim. Pellentesque sed dui ut augue blandit sodales. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam nibh. Mauris ac mauris sed pede pellentesque fermentum. Maecenas adipiscing ante non diam sodales hendrerit.'
                ],
                isLent: false
            },
            {
                title: 'Voyage au centre de la Terre',
                author: 'Jules Verne',
                description: [
                    'synopsis: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi. Proin porttitor, orci nec nonummy molestie, enim est eleifend mi, non fermentum diam nisl sit amet erat. Duis semper. Duis arcu massa, scelerisque vitae, consequat in, pretium a, enim. Pellentesque congue. Ut in risus volutpat libero pharetra tempor. Cras vestibulum bibendum augue. Praesent egestas leo in pede. Praesent blandit odio eu enim. Pellentesque sed dui ut augue blandit sodales. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam nibh. Mauris ac mauris sed pede pellentesque fermentum. Maecenas adipiscing ante non diam sodales hendrerit.'
                ],
                isLent: false
            },
            {
                title: 'De la Terre à la Lune',
                author: 'Jules Verne',
                description: [
                    'synopsis: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi. Proin porttitor, orci nec nonummy molestie, enim est eleifend mi, non fermentum diam nisl sit amet erat. Duis semper. Duis arcu massa, scelerisque vitae, consequat in, pretium a, enim. Pellentesque congue. Ut in risus volutpat libero pharetra tempor. Cras vestibulum bibendum augue. Praesent egestas leo in pede. Praesent blandit odio eu enim. Pellentesque sed dui ut augue blandit sodales. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam nibh. Mauris ac mauris sed pede pellentesque fermentum. Maecenas adipiscing ante non diam sodales hendrerit.'
                ],
                isLent: false
            },
            {
                title: 'Vingt mille lieues sous les mers',
                author: 'Jules Verne',
                description: [
                    'synopsis: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi. Proin porttitor, orci nec nonummy molestie, enim est eleifend mi, non fermentum diam nisl sit amet erat. Duis semper. Duis arcu massa, scelerisque vitae, consequat in, pretium a, enim. Pellentesque congue. Ut in risus volutpat libero pharetra tempor. Cras vestibulum bibendum augue. Praesent egestas leo in pede. Praesent blandit odio eu enim. Pellentesque sed dui ut augue blandit sodales. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam nibh. Mauris ac mauris sed pede pellentesque fermentum. Maecenas adipiscing ante non diam sodales hendrerit.'
                ],
                isLent: false
            },
            {
                title: 'Le Tour du monde en quatre-vingts jours',
                author: 'Jules Verne',
                description: [
                    'synopsis: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi. Proin porttitor, orci nec nonummy molestie, enim est eleifend mi, non fermentum diam nisl sit amet erat. Duis semper. Duis arcu massa, scelerisque vitae, consequat in, pretium a, enim. Pellentesque congue. Ut in risus volutpat libero pharetra tempor. Cras vestibulum bibendum augue. Praesent egestas leo in pede. Praesent blandit odio eu enim. Pellentesque sed dui ut augue blandit sodales. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam nibh. Mauris ac mauris sed pede pellentesque fermentum. Maecenas adipiscing ante non diam sodales hendrerit.'
                ],
                isLent: false
            }
        ];
        this.CdList = [
            {
                title: 'The College Dropout',
                artist: 'Kanye West',
                description: [
                    'genre: rap',
                    'auteur: Kanye West',
                    'compositeur: Kanye West'
                ],
                isLent: false
            },
            {
                title: 'Late Registration',
                artist: 'Kanye West',
                description: [
                    'genre: rap',
                    'auteur: Kanye West',
                    'compositeur: Kanye West'
                ],
                isLent: false
            },
            {
                title: 'Graduation',
                artist: 'Kanye West',
                description: [
                    'genre: rap',
                    'auteur: Kanye West',
                    'compositeur: Kanye West'
                ],
                isLent: false
            },
            {
                title: 'My Beautiful Dark Twisted Fantasy',
                artist: 'Kanye West',
                description: [
                    'genre: rap',
                    'auteur: Kanye West',
                    'compositeur: Kanye West'
                ],
                isLent: false
            },
            {
                title: 'Viva la Vida or Death and All His Friends',
                artist: 'Coldplay',
                description: [
                    'genre: pop alternative',
                    'auteur: Chris Martin',
                    'compositeur: Chris Martin'
                ],
                isLent: false
            },
            {
                title: 'Mylo Xyloto',
                artist: 'Coldplay',
                description: [
                    'genre: rap',
                    'auteur: Chris Martin',
                    'compositeur: Chris Martin'
                ],
                isLent: false
            }
        ];
    }
    DonneesService.prototype.emitBooks = function () {
        this.books$.next(this.bookList.slice());
    };
    DonneesService.prototype.emitCds = function () {
        this.cds$.next(this.CdList.slice());
    };
    DonneesService.prototype.lendBook = function (book, borrower) {
        book.borrower = borrower;
        book.isLent = true;
        this.saveBooksToLocalStorage();
        this.emitBooks();
    };
    DonneesService.prototype.renderBook = function (book) {
        book.borrower = null;
        book.isLent = false;
        this.saveBooksToLocalStorage();
        this.emitBooks();
    };
    DonneesService.prototype.lendCd = function (cd, borrower) {
        cd.borrower = borrower;
        cd.isLent = true;
        this.saveCdsToLocalStorage();
        this.emitCds();
    };
    DonneesService.prototype.renderCd = function (cd) {
        cd.borrower = null;
        cd.isLent = false;
        this.saveCdsToLocalStorage();
        this.emitCds();
    };
    DonneesService.prototype.saveBooksToLocalStorage = function () {
        this.storage.set('books', this.bookList);
    };
    DonneesService.prototype.saveCdsToLocalStorage = function () {
        this.storage.set('cds', this.CdList);
    };
    DonneesService.prototype.fetchBookList = function () {
        var _this = this;
        this.storage.get('books').then(function (list) {
            if (list && list.length) {
                _this.bookList = list.slice();
            }
            _this.emitBooks();
        });
    };
    DonneesService.prototype.fetchCdList = function () {
        var _this = this;
        this.storage.get('cds').then(function (list) {
            if (list && list.length) {
                _this.CdList = list.slice();
            }
            _this.emitCds();
        });
    };
    DonneesService.prototype.saveBooksToFirebase = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            __WEBPACK_IMPORTED_MODULE_2_firebase__["database"]().ref('books').set(_this.bookList).then(function (data) {
                resolve(data);
            }, function (error) {
                reject(error);
            });
        });
    };
    DonneesService.prototype.saveCdsToFirebase = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            __WEBPACK_IMPORTED_MODULE_2_firebase__["database"]().ref('cds').set(_this.CdList).then(function (data) {
                resolve(data);
            }, function (error) {
                reject(error);
            });
        });
    };
    DonneesService.prototype.retrieveBooksFromFirebase = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            __WEBPACK_IMPORTED_MODULE_2_firebase__["database"]().ref('books').once('value').then(function (data) {
                _this.bookList = data.val();
                _this.saveBooksToLocalStorage();
                _this.emitBooks();
                resolve('Données des Livres récupérées avec succès!');
            }, function (error) {
                reject(error);
            });
        });
    };
    DonneesService.prototype.retrieveCdsFromFirebase = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            __WEBPACK_IMPORTED_MODULE_2_firebase__["database"]().ref('cds').once('value').then(function (data) {
                _this.CdList = data.val();
                _this.emitCds();
                resolve('Données des Cd récupérées avec succès!');
            }, function (error) {
                reject(error);
            });
        });
    };
    DonneesService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__ionic_storage__["b" /* Storage */]])
    ], DonneesService);
    return DonneesService;
}());

//# sourceMappingURL=donneesService.js.map

/***/ })

},[252]);
//# sourceMappingURL=main.js.map